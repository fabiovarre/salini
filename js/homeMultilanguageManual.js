function setValues(lang){
    setTextLanguage(lang);
    if(lang === 'it'){
        $('#v1_t').html('Solidità');
        $('#v1_c').html('Realizziamo infrastrutture che durano nel tempo con le quali promuoviamo lo sviluppo sostenibile per le generazioni attuali e future.');
        $('#v2_t').html('Eccellenza Realizzativa');
        $('#v2_c').html('Crediamo nella competenza, nel lavoro ben fatto che superi le aspettative dei clienti.');
        $('#v3_t').html('Trasparenza');
        $('#v3_c').html('Promuoviamo una condotta etica, aperta e trasparente con tutti i nostri stakeholder.');
        $('#v4_t').html('Rispetto');
        $('#v4_c').html('Rispettiamo le persone, le diversità, l\'ambiente');
        $('#link-it').addClass('active');
        
    }else{
        $('#v1_t').html('Solidity');
        $('#v1_c').html('We build infrastructures that last in time and promote sustainable development for current and future generations.');
        $('#v2_t').html('Excellence');
        $('#v2_c').html('We believe in competence and skill, in work that is well done and capable of exceeding client\'s expectations.');
        $('#v3_t').html('Transparency');
        $('#v3_c').html('We promote an ethical, open and transparent conduct with all our stakeholders.');
        $('#v4_t').html('Respect');
        $('#v4_c').html('We respect people, diversity and the environment.');
        $('#link-en').addClass('active');

    }

}

function setTextLanguage(lang) {
    var imgVision = document.getElementById('vision-img');
    var imgMission = document.getElementById('mission-img');
    switch (lang) {
      case 'it':
        if(isDiga()) {
            imgMission.src = 'assets/images/mission2.png';
        } else if(isNightBuilding() || isPonte()) {
            imgMission.src = 'assets/images/mission2s.png';
        } else if(isGalleria()){
            imgMission.src = 'assets/images/mission2_black.png';
        } else {
            imgMission.src = 'assets/images/mission2g.png';
        }

        if (isNightBuilding()){
            imgVision.src = 'assets/images/vision2-white.png';
        } else if(isGalleria()){
            imgVision.src = 'assets/images/vision2_black.png';
        } else {
            imgVision.src = 'assets/images/vision2.png'; 
        }
        
        break;
      case 'en':
        if(isDiga()) {
            imgMission.src = 'assets/images/mission2_eng.png';
        } else if(isNightBuilding() || isPonte()) {
            imgMission.src = 'assets/images/mission2s_eng.png';
        } else if(isGalleria()) {
            imgMission.src = 'assets/images/mission2_black_eng.png';
        } else{
            imgMission.src = 'assets/images/mission2w_eng.png';
        }
        if(isNightBuilding()) {
            imgVision.src = 'assets/images/vision2_white_eng.png';
        } else if(isGalleria()){
            imgVision.src = 'assets/images/vision2_black_eng.png';
        } else {
            imgVision.src = 'assets/images/vision2_eng.png';
        }
        
        break;
      default:

    }
}
