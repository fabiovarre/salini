/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var peoples = [];
var ONE_DAY = 1000 * 60 * 60 * 24;
var Person = function( name, role, story, quote, poster, link, image, shortcut, releaseDate, revealDate, gender, sov1){

    this.name = name;
    this.role = role;
    this.story = story;
    this.quote = quote;
    this.poster  = poster;
    this.link = link;
    this.image = image;
    this.shortcut = shortcut;
    this.releaseDate = releaseDate;
    this.revealDate = revealDate;
    this.gender = gender;
    this.sov1 = sov1;



};
//Insert template
//personMap.set('',new Person('','','','','','','',''));
var query = window.location.search;
var vars = query.split("=");

var personMap = new Map();

personMap.set('fiori', new Person(
    'Massimo Fiori',
    'Operational Manager – Domestic Operations, Area 4',
    'Massimo Fiori, ingegnere civile. La mia esperienza nel Gruppo comincia nel 1997, dapprima in una società partecipata e, dal 2009, direttamente in Azienda. La mia carriera nel settore della costruzione di opere pubbliche nel comparto dell’edilizia e degli impianti civili ed industriali inizia come responsabile della progettazione strutturale, di Project Manager fino a Direttore Tecnico. Attualmente ricopro l’incarico di Operational Manager – Domestic Operations per l’Area 4. Tra i principali cantieri di cui mi sono occupato, l\'Università degli Studi della Calabria e il Termovalorizzatore di Acerra. Grazie all’esperienza professionale acquisita, cerco di trasferire ai miei collaboratori l’importanza dei valori che caratterizzano il Gruppo, quali l’eccellenza realizzativa e la trasparenza.',
    '“Trovo soddisfazione per quello ogni giorno faccio e questo è lo stimolo che mi porta a superare qualsiasi ostacolo”.',
    'img/poster/poster_fiori.jpg',
    'people_2.html?people=fiori&lang=it',
    'assets/images/people/massimo_fiori.jpg',
    'fiori', 
    new Date(2017,8,26),
    new Date(2017,8,26),
    'M', false
));

personMap.set('ferrara', new Person(
    'Marco Ferrara',
    'HR Operations and Industrial Relations Director',
    'Marco Ferrara, laureato in Giurisprudenza. La mia carriera all\’interno del Gruppo inizia nel 2002. Nel corso del tempo ho via via ricoperto incarichi di sempre crescente responsabilità, spaziando nei vari ambiti HR ed arrivando ad assumere, nel 2014, anno della fusione del gruppo Salini Impregilo, il ruolo di Responsabile della Pianificazione, Selezione, Gestione del Personale e Relazioni Industriali. L\’anno successivo vengo nominato HR Operations and Industrial Relations Director, incarico che svolgo attualmente con passione e dedizione. Il percorso professionale e umano di questi 15 anni di azienda mi ha dato l\’opportunità di conoscere a fondo la Società e la complessità del mondo delle infrastrutture. In futuro auspico di contribuire alla crescita della Società attraverso la valorizzazione del patrimonio umano, asset fondamentale di questo grande Gruppo.',
    '“Quello su cui qualsiasi manager deve lavorare è la gestione delle risorse. Il responsabile della squadra è solo il direttore d\'orchestra”.',
    'img/poster/poster_ferrara.jpg',
    'people_2.html?people=ferrara&lang=it',
    'assets/images/people/marco_ferrara.jpg',
    'ferrara', 
    new Date(2017,9,02), 'M', false
));

personMap.set('buffa', new Person(
    'Alberto Buffa',
    'Plant and Equipment Department Director',
    'Alberto Buffa, oggi Plant and Equipment Director. Nel Gruppo dal 1984 quando ho iniziato a scoprire il mondo delle grandi opere nell\’allora più grande progetto idroelettrico in esecuzione, Yacyretà in Argentina. Ho avuto in seguito la fortuna di partecipare alle task force dei più rappresentativi progetti della nostra società come, tra gli altri, le dighe di Piedra del Aguila ed Ertan e l\’ampliamento del Canale di Panama. Veder nascere e avviare i sistemi produttivi dei grandi progetti ha alimentato la curiosità e l\’entusiasmo di sviluppare metodi di costruzione innovativi e sempre più efficienti mantenendo i più alti standard qualitativi, contribuendo a rendere il nostro Gruppo l\’attuale benchmark del settore.',
    '“Dobbiamo riuscire a portare il meglio di quello che abbiamo imparato precedentemente e mutuarlo in nuove sfide, in nuove attività”.',
    'img/poster/poster_buffa.jpg',
    'people_2.html?people=buffa&lang=it',
    'assets/images/people/albero_buffa.jpg',
    'buffa', 
    new Date(2017,8,12), 'M', false
));

personMap.set('laterza', new Person(
    'Maurizio La Terza',
    'Operational Manager – Domestic Operations, Area 2',
    'Maurizio La Terza, ingegnere civile. La mia carriera nell\’azienda inizia nel 1982, subito dopo l\’università, nei cantieri delle opere idrauliche e degli impianti di depurazione dapprima di Torino e quindi di Caserta, e prosegue negli USA nella realizzazione di centrali nucleari e poi a Montalto di Castro per l\’immersed tunnel di presa della centrale nucleare come Direttore di Cantiere. L\’esperienza nei lavori marittimi proseguirà con le opere di salvaguardia di Venezia per passare poi dal 2004 alla realizzazione del Passante di Mestre e dell\’ampliamento della A4 Venezia-Trieste come Project Manager. Attualmente ricopro il ruolo di Operational Manager - Domestic Operations per l\’Area 2 che comprende diverse commesse sia autostradali sia nell\’ambito della rete ferroviaria dell\’Alta Velocità.',
    '“Credo che questo sia il senso di quello che facciamo. Si coniugano queste cose: la concretezza, il fare, il costruire e il sogno”.',
    'img/poster/poster_laterza.jpg',
    'people_2.html?people=laterza&lang=it',
    'assets/images/people/maurizio_laterza.jpg',
    'laterza', 
    new Date(2017,8,05), 'M', false
));

personMap.set('basso', new Person(
    'Ernesta Basso',
    'Accounting, Tax and Statutory Reporting Director',
    'Ernesta Basso, da due anni ricopro il ruolo di Direttore dell\’Accounting, Tax and Statutory Reporting Department. Il mio dipartimento ha in carico una serie di attività, dalla supervisione della contabilità alla redazione della Relazione Finanziaria semestrale ed annuale, fino alla gestione dei rapporti con la Società di Revisione e gli Organismi di Controllo ed al supporto alle altre Direzioni. Attività che, inserite nel contesto complesso di un Gruppo che opera in circa 50 paesi, creano la continua necessità di adattarsi alle varie realtà locali.',
    '“Le nostre opere producono non solo progresso ma anche benessere, economico e sociale. Aprire e chiudere una commessa è come aprire e chiudere un\’azienda”',
    'img/poster/poster_basso.jpg',
    'people_2.html?people=basso&lang=it',
    'assets/images/people/ernesta_basso.jpg',
    'basso', 
    new Date(2017,8,19), 'F', false
));

personMap.set('mirabelli', new Person(
    'Alessandro Mirabelli',
    'Planning, Budget & Reporting Director',
    'Alessandro Mirabelli, ho 45 anni, oggi Planning, Budget & Reporting Director di Salini Impregilo. La mia esperienza nel Gruppo è iniziata nel 1995. Dopo aver conseguito la laurea alla University of London nel 1992 e aver avviato la mia esperienza professionale in Swaziland e USA, sono entrato in azienda nella Direzione Amministrazione e Finanza. Dapprima ho lavorato nel Budgeting & Reporting, poi, dal 2004, ho seguito in qualità di Project Manager il processo di rating aziendale fino a diventare nel 2009 responsabile del Bilancio redatto secondo gli IAS / IFRS. Dal 2011 ho ricoperto il ruolo di Responsabile Servizio Budget & Reporting (SBR) con main focus su Reporting aziendale e redazione del business plan aziendale. Un percorso lavorativo che mi ha permesso di crescere professionalmente ed umanamente.',
    '“La lezione che ho imparato all’interno del Gruppo è quella di avere fiducia in chi mi sta guidando. Oggi sto trasferendo questa fiducia ai miei collaboratori, facendo capire loro quant&#39;è importante lavorare insieme per un obiettivo comune”.',
    'img/poster/poster_mirabelli.jpg',
    'people_2.html?people=mirabelli&lang=it',
    'assets/images/people/alessandro_mirabelli.jpg',
    'mirabelli', 
    new Date(2017,6,31), 'M', false
));

personMap.set('blanda', new Person(
    'Leonardo Blanda',
    'Area Coordinator – International Operations',
    'Leonardo Blanda, ingegnere civile. Nel Gruppo dal 1987, attualmente ricopro il ruolo di Executive Director e Area Coordinator nell\’ambito della Direzione International Operations per diversi paesi del Medio Oriente e dell\’Europa, occupandomi di progetti dove leadership, flessibilità e problem solving sono qualità fondamentali per la proficua gestione del lavoro. La mia carriera è stata caratterizzata dalla realizzazione di progetti stradali (Rabat-Larache, Marocco), centrali idroelettriche (il sistema Gibe, Etiopia) e metropolitane (Red Line, Doha e Metro Linea 3, Riyadh). Il progetto che più di tutti rappresenta per me motivo d\’orgoglio è sicuramente il Cityringen di Copenhagen che, inserito in un tessuto urbano denso e vitale, attraversa tutto il centro storico della città.',
    '“La mia carriera mi ha dato grande soddisfazione, perché mi ha portato a seguire diversi progetti che rappresentano l\’eccellenza a livello mondiale. Sapevo che dovevo fare l\’ingegnere, era la mia aspirazione fin da bambino”.',
    'img/poster/poster_blanda.jpg',
    'people_2.html?people=blanda&lang=it',
    'assets/images/people/leonardo_blanda.jpg',
    'blanda', 
    new Date(2017,6,25), 'M', false
));

personMap.set('catrini', new Person(
    'Gianfranco Catrini',
    'Corporate Development and Subsidiaries Director',
    'Gianfranco Catrini, 44 anni. La mia esperienza in Salini Impregilo è cominciata nel 1996. Dapprima in Pakistan seguendo la commessa del Ghazi Barotha, poi in Nepal presso il cantiere di Kali Gandaki. Dopo vari incarichi in diverse società del Gruppo, tra cui Group Finance Director UK nel 2002, Managing Director di Impregilo International Infrastructures NV in Olanda nel 2008, Managing Director  di Fisia Babcock Environment GmbH in Germania nel 2009, e board member di varie società del Gruppo in Argentina, Brasile, Cina ed Europa, nel 2012 sono stato nominato Direttore del Corporate Development con l\’obiettivo di dismettere prima le attività non strategiche e poi di fare le acquisizioni per accompagnare la crescita della società. Dal 2016, ricopro l\’incarico di Direttore del Corporate Development and Subsidiaries, coordinando le attività di M&A e delle partecipate del Gruppo e le attività di integrazione, coordinamento e controllo della Lane Construction.',
    '“Negli anni, nel tempo, viviamo un continuo stimolo e cambio di attività, di lavoro, di prospettiva, di luogo, di persone, di situazioni”.',
    'img/poster/poster_catrini.jpg',
    'people_2.html?people=catrini&lang=it',
    'assets/images/people/gianfranco_catrini.jpg',
    'catrini', 
    new Date(2017,6,18), 'M', false));

personMap.set('fabbri', new Person(
    'Bruno Fabbri',
    'Technical Director',
    'Bruno Fabbri, Ingegnere Civile. Nel passato ho avuto la fortuna di vivere una lunga e ricca esperienza umana e professionale, piena di traguardi raggiunti e poi superati numerose volte, in prima linea su cantieri come Bumbuna, in Sierra Leone, e Tana Beles, in Etiopia, e poi in sede sempre con la nostra Azienda. Nel presente e nel futuro l’obiettivo di ottimizzare e potenziare l\’operatività del Technical Department, attraverso progetti come il Cityringen di Copenaghen, al fine di garantire all\’Azienda lo sviluppo programmato in un contesto internazionale in continua, profonda e rapida evoluzione.',
    '“Il nostro è un mestiere che pone delle sfide e se non si è desiderosi di accettarle probabilmente manca qualcosa che rende determinante e di successo la nostra attività”.',
    'img/poster/poster_fabbri.jpg',
    'people_2.html?people=fabbri&lang=it',
    'assets/images/people/bruno_fabbri.jpg',
    'fabbri', 
    new Date(2017,6,11), 'M', false));

personMap.set('albieri', new Person(
    'Francesco Albieri',
    'Internal Audit & Compliance Director',
    'Francesco Albieri, 48 anni. In Impregilo dal 1998 al 2000, dopo un decennio in Parmalat rientro nel 2014 in Salini Impregilo come Responsabile Internal Audit. Attualmente ricopro l\’incarico di Capo della Direzione Internal Audit & Compliance. Con il mio team di professionisti mi impegno affinché il nostro lavoro venga percepito come di supporto ai colleghi e considero dote indispensabile la capacità di trovare un punto di incontro tra opinioni diverse. Ogni attività svolta è fonte continua di approfondimenti e confronti. Questa trasversale conoscenza del Gruppo consente alla nostra Direzione anche di diventare “portavoce di knowledge aziendale” tra le diverse funzioni.',
    '“La cosa che mi emoziona di più nel mio lavoro è quella di riuscire a vedere così tanti processi grazie all\’attività che facciamo. Siamo fortunati, vediamo cose diverse, fatte in maniera diversa”.',
    'img/poster/poster_albieri.jpg',
    'people_2.html?people=albieri&lang=it',
    'assets/images/people/francesco_albieri.jpg',
    'albieri', new Date(2017,6,4), 'M', false));

personMap.set('carlesimo',new Person('Leopoldo Carlesimo','Area Coordinator – International Operations','Leopoldo Carlesimo, dopo una laurea in Ingegneria alla Sapienza di Roma mi specializzo in analisi strutturale computerizzata. Nel 1996 la Salini mi affida il compito di seguire gli aspetti tecnici delle commesse estere dell’azienda. Nella mia carriera mi sono occupato con passione soprattutto della costruzione di dighe, le opere ingegneristiche più belle, complete e interessanti, non solo per la componente prototipica ma soprattutto per il positivo impatto sociale, culturale, ed economico che hanno sul territorio. Ho lavorato a progetti quali la diga di M’Jaara in Marocco, la diga di Gurara in Nigeria e di Garafiri in Guinea.','“La diga è l’opera più bella, la più completa”',
                                    'img/poster/poster_carlesimo.jpg','people_2.html?people=carlesimo&lang=it',
                                    'assets/images/people/leopoldo_carlesimo.jpg','carlesimo', new Date(2017,03,02), 'M', true));
personMap.set('quarta',new Person('Giuseppe Quarta','Area Manager – International Operations','Giuseppe Quarta, ingegnere civile, debutto nella Società agli inizi degli anni ‘80 nei cantieri di Alicurá, Piedra del Águila e Yacyretá in America latina. Dal 1991 sono in Cina in qualità di Deputy Project Manager del cantiere per la diga di Ertan, per poi occuparmi dello sviluppo commerciale dell’azienda nell’area asiatica e negli USA, paese in cui ho rivestito il ruolo di Area Manager per progetti fra i quali il West Side Tunnel di Portland. Attualmente vivo a Panama e sono CEO del GUPC (Grupo Unidos por el Canal) per l’ampliamento del Canale di Panama, terzo set di chiuse.','“L\'eccellenza nel nostro lavoro vuol dire riuscire a fare un passo avanti nella storia del nostro mestiere”',
                                  'img/poster/poster_quarta.png','people_2.html?people=quarta&lang=it',
                                  'assets/images/people/giuseppe_quarta.jpg','quarta', new Date(2017,03,02), 'M', true));
personMap.set('zaffaroni',new Person('Antonio Zaffaroni','Area Coordinator – International Operations','Antonio Maria Zaffaroni, ingegnere. La mia lunga carriera comincia nel 1975 nella Società a capo di grandi progetti esteri, come la diga di Salto Grande in Uruguay, la diga di Yacyretà al confine tra Argentina e Paraguay e la diga di Ertan nella regione cinese del Sichuan. Nel 2012 accetto l’incarico di Area Manager per le Americhe con l’intento di contribuire con la mia esperienza alla crescita dell’azienda. La soddisfazione più grande: mantenere livelli qualitativi eccellenti nel portare a termine una grande opera.','”A sei anni dissi ai miei genitori: Io da grande farò l’ingegnere e andrò in giro per il mondo a costruire dighe”.',
                                    'img/poster/poster_zaffaroni.png','people_2.html?people=zaffaroni&lang=it',
                                    'assets/images/people/antonio_zaffaroni.jpg','zaffaroni', new Date(2017,04,1,23,00), 'M', true));
personMap.set('assorati',new Person('Marco Assorati','Area Manager – International Operations','Marco Assorati, ingegnere civile. Sin dal 1998 ricopro posizioni di rilievo all’interno della Società,occupandomi della realizzazione di grandi opere nel mondo: dalla Guinea Conakry con il Progetto Stradale Kouroussa Kankan, alla Malesia, paese in cui opero attualmente in qualità di Project Manager del progetto idroelettrico Ulu Jelai, passando per l’Etiopia con la diga Gibe I e la Nigeria, con il progetto idroelettrico di Gurara. La mia lunga permanenza all’estero mi ha fatto capire quanto conti in questo lavoro la capacità di comprendere culture diverse dalla nostra.','“Nella nostra professione siamo molto esposti al contatto con persone, culture e religioni diverse e siamo abituati a superare tanti ostacoli con il dialogo”.',
                                    'img/poster/poster_assorati.jpg','people_2.html?people=assorati&lang=it',
                                    'assets/images/people/marco_assorati.jpg','assorati', new Date(2017,04,01), 'M', true));
personMap.set('zoppis',new Person('Eugenio Zoppis','Site Project Manager','Eugenio Zoppis, ingegnere civile. Ho iniziato la mia carriera nell’Azienda nel 1988 assumendo presto il ruolo di Project Manager per importanti cantieri stradali ed idroelettrici in territorio africano. Un’intera carriera in viaggio tra Sudan (Khartoum - Port Sudan Road), Etiopia (dighe e gallerie a Gibe I e Gibe II) e Sierra Leone (diga di Bumbuna) oltre alla mastodontica diga Gibe III in Etiopia, che seguo dal 2011 in qualità di Project Manager. In contesti entusiasmanti ma difficili come l’Etiopia, ciò che fa veramente la differenza sono dedizione, organizzazione e problem solving, che devono diventare qualità intrinseche alla personalità di chi fa questo lavoro.','“Noi sappiamo di aver portato un cambiamento nelle condizioni di un’intera nazione. Abbiamo portato innanzitutto lavoro, progetti di questo tipo riescono a impiegare circa 6.000/7.000 persone”.',
                                    'img/poster/poster_zoppis.png','people_2.html?people=zoppis&lang=it',
                                    'assets/images/people/eugenio_zoppis.jpg','zoppis', new Date(2017,04,01), 'M', true));
personMap.set('mariotti',new Person('Mirta Mariotti','Procurement Specialist','Mirta Mariotti, architetto romano. La mia esperienza nel Gruppo inizia nel 2007 come consulente su temi architettonici, entrando nell’organico dell’azienda per seguire gli acquisti delle finiture architettoniche del Millennium Tower e Cultural Center in Nigeria. A partire dal 2012 mi trasferisco prima a Copenaghen in qualità di Vendors Manager per il progetto Cityringen Metro, poi a Doha in Qatar per il progetto Red Line North Underground, come Procurement Specialist di impianti elettromeccanici e finiture architettoniche. Per un giovane, poter lavorare in un cantiere estero rappresenta un’opportunità di crescita professionale ed umana irripetibile.','“L’azienda nei confronti dei giovani è molto attenta. Un giovane deve assolutamente provare l’esperienza del cantiere all’estero: dal punto di vista professionale è l’unico modo per crescere veramente”.',
                                    'img/poster/poster_mariotti.png','people_2.html?people=mariotti&lang=it',
                                    'assets/images/people/mirta_mariotti.jpg','mariotti', new Date(2017,04,01), 'F', true));
personMap.set('saraceni',new Person(
    'Marco Saraceni',
    'Branch Manager',
    'Marco Saraceni, ingegnere civile. Dal 1987 ho maturato la mia esperienza nella realizzazione di grandi progetti di infrastrutture fra cui le dighe di Little Beles in Etiopia, di Bumbuna in Sierra Leone, e come Site Manager, le dighe di Zhovhe e Osborne in Zimbabwe, Garafiri in Guinea, ma anche progetti stradali, come la Kouroussa-Kankan in Guinea, il nodo autostradale urbano dell\'Interchange n.1 di Dubai ed un lotto della Silk Road in Kazakistan. Più recentemente ho ricoperto il ruolo di Project Manager per l \'avvio della costruzione di un importante lotto dell\'Autostrada D1 in Slovacchia. Nel nostro lavoro, per realizzare opere sempre più complesse che mobilizzano quantità di risorse importanti, è indispensabile capacità di ascolto, rapidità di giudizio e di decisione.',
    '”Un aspetto bello di questo lavoro è la complessità, alla quale bisogna rispondere non solo con le proprie forze ma con le conoscenze di tutta la squadra che viene organizzata per portare avanti un progetto”.',
    'img/poster/poster_saraceni.png',
    'people_2.html?people=saraceni&lang=it',
    'assets/images/people/marco_saraceni.jpg',
    'saraceni', 
    new Date(2017,03,02), 'M', true));

personMap.set('cimiotti',new Person('Claudio Cimiotti','Senior Tunnel Engineer','Claudio Cimiotti, ingegnere gestionale. Inizio la mia esperienza in Azienda nel 2006 dopo un breve periodo di praticantato in ambito Cost Control. La mia rapida carriera mi porta nel giro di qualche anno ad occuparmi di due importanti cantieri fra Sud America e Stati Uniti: quello della diga Mazar in Ecuador, in qualità di Responsabile del Cost Control, e quello del Tunnel idraulico di Lake Mead Intake No.3 a Las Vegas come Senior Tunnel Engineer.Le technical skills che ho acquisito negli anni trascorsi in America stanno giocando un ruolo fondamentale per il raggiungimento dei miei obiettivi personali e lavorativi.','“Lake Mead è un punto di riferimento nel settore del tunneling a livello mondiale. Le difficoltà idrogeologiche e le pressioni alle quali ha dovuto scavare la TBM ci hanno spinto a sviluppare nuove tecnologie e nuovi processi”.',
                                    'img/poster/poster_cimiotti.jpg','people_2.html?people=cimiotti&lang=it',
                                    'assets/images/people/claudio_cimiotti.jpg','cimiotti', new Date(2017,03,02), 'M', true));
personMap.set('leghissa',new Person('Luca Legisa Leghissa','Project Manager','Luca Legisa Leghissa, ingegnere triestino. Sin dal 2006 entro in Azienda nel settore commerciale seguendo importanti progetti come in Nigeria (Abuja – Area Industriale di IDU), e in Danimarca (Cityringen Metro), certamente il progetto di maggior soddisfazione nella mia carriera. Dal 2011 sono in Cile come Branch Manager seguendo varie iniziative commerciali in tutta l\'America Latina. Attualmente rivesto il ruolo di Project Manager per la Metro Lima Linea 2 in Perù. Il cemento più forte è il legame professionale che si crea nel team di lavoro in cantiere.','“Tutte le persone che stanno all’interno del cantiere sono indispensabili alla vita del cantiere. Nelle realtà in cui operiamo, il far gruppo è la base per la riuscita di qualunque progetto”.',
                                    'img/poster/poster_leghissa.png','people_2.html?people=leghissa&lang=it',
                                    'assets/images/people/luca_legisa.jpg','leghissa', new Date(2017,03,02), 'M', true));



personMap.set('chirico',new Person('Federica Chirico','Addetto supporto QHSE','Federica Chirico, dopo una laurea in Architettura alla Sapienza ottengo il diploma di Master in Edifici e Infrastrutture Sostenibili presso il Politecnico di Milano. Durante gli studi la Società mi seleziona per seguire il cantiere di New Kutaisi Bypass in Georgia nel ruolo di Responsabile per la Qualità, per poi integrarmi nell’organico di sede come supporto Qualità, Ambiente e Sicurezza per i cantieri est-europei del Gruppo. Affrontando continuamente sfide lavorative in cui contano tenacia, dedizione e serietà, si diventa più consapevoli del proprio contributo nella costruzione di valore per le popolazioni nel mondo.','“Il nostro motto è vero, è vero che noi costruiamo valore. Valore per quella che è la vita e il commercio delle popolazioni che fruiranno della nostra opera.”',
                                    'img/poster/poster_chirico.jpg','people_2.html?people=chirico&lang=it',
                                    'assets/images/people/chirico_bg.png','chirico', new Date(2017,03,02), 'F', true));
let unkPerson = new Person('','','','','','','','',new Date(2017,03,01),'', false);





var personMapEn = new Map();
personMapEn.set('albieri', new Person(
    'Francesco Albieri',
    'Internal Audit & Compliance Director',
    'Francesco Albieri, 48. Worked for Impregilo from 1998 to 2000. After spending ten years at Parmalat, I re-joined Salini Impregilo in 2014 as Internal Audit Manager. My current job is Internal Audit & Compliance Director. With my team of professionals, I strive to make sure that our job is perceived as support tool by colleagues and consider it fundamental to be able to find common ground between different opinions. Each assignment is always an opportunity for in-depth analysis and discussion. This cross-functional knowledge of the Group allows our Department to also act as the “disseminator of corporate knowledge” among various functions.',
    '“What excites me most about my job is that I am able to see so many different processes thanks to what we do. We are lucky; we see different things done in a different way”.',
    'img/poster/poster_albieri.jpg',
    'people_2.html?people=albieri&lang=en',
    'assets/images/people/francesco_albieri.jpg',
    'albieri', new Date(2017,6,4), 'M', false
));
personMapEn.set('fabbri', new Person(
    'Bruno Fabbri',
    'Technical Director',
    'Bruno Fabbri, Civil Engineer. I have been lucky enough to have a long, rich personal and professional experience, full of goals that I have managed to reach and, many times, exceed, first of all at sites like Bumbuna, in Sierra Leone, and Tana Beles, in Ethiopia, and then working at our corporate headquarters. My current and future goal is that of optimising and enhancing the operational capabilities of the Technical Department, through projects such as the Copenhagen Cityringen, in order to ensure the planned growth of the Company against an international backdrop that is experiencing ongoing, profound and rapid change.',
    '“Our job is full of challenges and if you are not prepared to step up to them, then you lack something that makes our activity crucial and successful”.',
    'img/poster/poster_fabbri.jpg',
    'people_2.html?people=fabbri&lang=en',
    'assets/images/people/bruno_fabbri.jpg',
    'fabbri', 
    new Date(2017,6,11), 'M', false));

personMapEn.set('catrini', new Person(
    'Gianfranco Catrini',
    'Corporate Development and Subsidiaries Director',
    'Gianfranco Catrini, 44 years old. I started working for Salini Impregilo in 1996. First in Pakistan, on the Ghazi Barotha project, then in Nepal, at the Kali Gandaki site. After working in different capacities for various Group companies, including Group Finance Director UK in 2002, Managing Director of Impregilo International Infrastructures NV in Holland in 2008, Managing Director of Fisia Babcock Environment GmbH in Germany in 2009, and board member of various Group companies in Argentina, Brazil, China and Europe, in 2012 I was appointed Corporate Development Director tasked with first divesting non-strategic companies and then making acquisitions to contribute to the Company’s growth. I have been Corporate Development and Subsidiaries Director since 2016 and I am responsible for the coordination of M&A activities and the Group’s subsidiaries’ activities and the integration, coordination and control of Lane Construction.',
    '“Over time, we have experienced constant inspiration and change in terms of roles, jobs, opportunities, places, people and situations”.',
    'img/poster/poster_catrini.jpg',
    'people_2.html?people=catrini&lang=en',
    'assets/images/people/gianfranco_catrini.jpg',
    'catrini', 
    new Date(2017,6,18), 'M', false));

personMapEn.set('blanda', new Person(
    'Leonardo Blanda',
    'Area Coordinator – International Operations',
    'Leonardo Blanda, Civil Engineer. I have been with the Group since 1987. At the moment I work as Executive Director and Area Coordinator for the International Operations and I am responsible for various countries in the Middle East and Europe, working on projects where leadership, flexibility and problem-solving are key to the successful management of work. My career has been characterised by the development of motorway (Rabat-Larache, Morocco), hydroelectric plant (the Gibe system, Ethiopia) and metro projects (Red Line, Doha and Metro Line 3, Riyadh). The project that I am most proud of is without a doubt the Copenhagen Cityringen, which is part of a very densely populated, vital urban environment and cuts across the entire historic city centre.',
    '“My career has given me many satisfactions because it has enabled me to manage various projects that represent global excellence. I knew I had to be an engineer. It was my dream as a child”.',
    'img/poster/poster_blanda.jpg',
    'people_2.html?people=blanda&lang=en',
    'assets/images/people/leonardo_blanda.jpg',
    'blanda', 
    new Date(2017,6,25), 'M', false
));

personMapEn.set('mirabelli', new Person(
    'Alessandro Mirabelli',
    'Planning, Budget & Reporting Director',
    'Alessandro Mirabelli, I am 45 years, and currently the Planning, Budget & Reporting Director of Salini Impregilo. I have been working in the Group since 1995. After graduating from the University of London in 1992, and having begun my professional experience in Swaziland and USA, I started to work in the company\'s Administration and Finance Department. I began working in the Budgeting & Reporting function. Then, from 2004, I followed as Project Manager the company\'s rating process until 2009, when I became the person responsible for drawing-up the financial statements according to the IAS/IFRS. Since 2011, I have covered the role of Budget & Reporting Director, mainly focusing on corporate reporting and on drawing-up the company\'s business plan.  My work path allowed me to grow both professionally and as a human being.',
    '“This Group has taught me to have faith in who\'s guiding me. Today, I am transferring this trust to my team, to my collaborators, letting them understand the importance of working together to reach a common objective”.',
    'img/poster/poster_mirabelli.jpg',
    'people_2.html?people=mirabelli&lang=en',
    'assets/images/people/alessandro_mirabelli.jpg',
    'mirabelli', 
    new Date(2017,6,31), 'M', false
));

personMapEn.set('basso', new Person(
    'Ernesta Basso',
    'Accounting, Tax and Statutory Reporting Director',
    'Ernesta Basso, I have been working as Accounting, Tax and Statutory Reporting Director for two years. My department is responsible for a range of activities, from supervising the accounts to preparing the half-year and annual Financial Report, through to managing the relationship with the Independent Statutory Auditor and Supervisory Authorities and supporting other Departments. These activities take place in the complex environment of a Group that operates in around 50 countries, which creates the need to continuously adapt to local conditions.',
    '“Our works create not only progress, but also welfare, both economic and social. Opening and closing a project is like setting up and shutting down a business".',
    'img/poster/poster_basso.jpg',
    'people_2.html?people=basso&lang=en',
    'assets/images/people/ernesta_basso.jpg',
    'basso', 
    new Date(2017,8,19), 'F', false
));
personMapEn.set('laterza', new Person(
    'Maurizio La Terza',
    'Operational Manager – Domestic Operations, Area 2',
    'Maurizio La Terza, Civil Engineer. I started working for the Group in 1982, immediately after finishing university, on hydraulic works and treatment plants, first in Turin, then in Caserta. I then moved to the USA to work on nuclear power stations, to go back to Italy, to Montalto di Castro, where I was Site Manager of the nuclear plant’s immersed intake tunnel. My experience in marine works continued with the Venice conservation project and then, in 2004, I became Project Manager for the Mestre Bypass project and the expansion of the A4 Venice-Trieste Motorway. I am currently Operational Manager - Domestic Operations for Area 2, which includes various motorway and high-speed railway projects.',
    '“I believe that this is the meaning of what we do. We combine these things: concreteness – making, building – and dreaming”.',
    'img/poster/poster_laterza.jpg',
    'people_2.html?people=laterza&lang=en',
    'assets/images/people/maurizio_laterza.jpg',
    'laterza', 
    new Date(2017,8,05), 'M', false
));
personMapEn.set('buffa', new Person(
    'Alberto Buffa',
    'Plant and Equipment Department Director',
    'Alberto Buffa, I am currently Plant and Equipment Director. I joined the Group in 1984, when I started to discover the world of large-scale works at the then biggest hydroelectric project under construction, Yacyretà in Argentina. Then, I was lucky enough to be a member of the task forces working on some of our Company’s most representative projects, including Piedra del Aguila and Ertan dams and the expansion of the Panama Canal. Witnessing the construction and start-up stages of large-scale works fuelled my curiosity and the passion for developing innovative and more efficient building methods whilst maintaining the highest quality standards, contributing to making our Group the current industry benchmark.',
    '“We must always be able to take the best of what we have done before and exploit it for new challenges, new activities”.',
    'img/poster/poster_buffa.jpg',
    'people_2.html?people=buffa&lang=en',
    'assets/images/people/albero_buffa.jpg',
    'buffa', 
    new Date(2017,8,12), 'M', false
));
personMapEn.set('ferrara', new Person(
    'Marco Ferrara',
    'HR Operations and Industrial Relations Director',
    'Marco Ferrara, Law Degree. My career within the Group started in 2002. Over time, I took on increasingly greater responsibilities, working in various areas of HR and becoming HR and Industrial Relations Manager in 2014, the year of the merger of the Salini Impregilo Group. The following year I was appointed HR Operations and Industrial Relations Director, a role that I am currently performing with passion and dedication. My professional and human development in the 15 years I have spent as part of the organisation has given me the opportunity to get to know the Company and the complexities of the infrastructure sector as well. In the future, I hope to be able to contribute to the growth of the Group through the development of its human capital, which is a key asset for Salini Impregilo.',
    '“What every manager needs to work on is how to manage people. The team leader is only the orchestra conductor”.',
    'img/poster/poster_ferrara.jpg',
    'people_2.html?people=ferrara&lang=en',
    'assets/images/people/marco_ferrara.jpg',
    'ferrara', 
    new Date(2017,9,3), 'M', false
));
personMapEn.set('fiori', new Person(
    'Massimo Fiori',
    'Operational Manager – Domestic Operations, Area 4',
    'Massimo Fiori, Civil Engineer. My experience with the Group started in 1997, initially working for an investee company and then, since 2009, directly for the Company. I started my career in the public works and civil and industrial facilities sector as Structural Designer, then progressed to Project Manager and Technical Director. At the moment I work as Operational Manager – Domestic Operations for Area 4. The main projects I have worked on include the University of Calabria and the Acerra Urban Solid Waste Treatment Plant. Thanks to my professional experience, I try to pass on to my co-workers the importance of the values that characterise the Group, such as excellence and transparency. ',
    '"I am very fulfilled by what I do on a day-to-day basis and let\'s say that this is what motivates me to overcome any hurdles".',
    'img/poster/poster_fiori.jpg',
    'people_2.html?people=fiori&lang=en',
    'assets/images/people/massimo_fiori.jpg',
    'fiori', 
    new Date(2017,8,26), 'M', false
));

personMapEn.set('carlesimo',new Person('Leopoldo Carlesimo','Area Coordinator – International Operations','Leopoldo Carlesimo. After graduating from La Sapienza University of Rome in Engineering, I specialised in computerised structural analysis. In 1996 Salini entrusted me with the task of following the technical aspects of foreign contracts of the company. During my career I have enthusiastically dealt with, above all, the construction of dams, the most beautiful, complete and interesting engineering works, not only because of the prototype component, but especially due to the positive social, cultural and economic impact they have. I have worked on projects like the M’Jaara Dam in Morocco, the Gurara dam in Nigeria and the Garafiri dam in Guinea.','“Dams are the most beautiful, most complete infrastructures”',
                                    'img/poster/poster_carlesimo.jpg','people_2.html?people=carlesimo&lang=en',
                                    'assets/images/people/leopoldo_carlesimo.jpg','carlesimo', new Date(2017,04,01), 'M', true));
personMapEn.set('quarta',new Person('Giuseppe Quarta','Area Manager – International Operations','Giuseppe Quarta, Civil Engineer. I joined the Company in the early 1980s, to work in Latin America on the construction sites of Alicurá, Piedra del Águila and Yacyretá. In 1991, I headed to China as Deputy Project Manager for the Ertan dam site, later taking on the role of business development manager for the company in Asia and in the USA, where I was Area Manager for projects such as the West Side Tunnel of Portland. I currently reside in Panama and am CEO of the Consortium GUPC (Grupo Unidos por el Canal) for extension of the Panama Canal, third set of locks.','“Excellence in our work means making strides in the history of our profession.”',
                                    'img/poster/poster_quarta.png','people_2.html?people=quarta&lang=en',
                                    'assets/images/people/giuseppe_quarta.jpg','quarta', new Date(2017,04,01), 'M', true));
personMapEn.set('zaffaroni',new Person('Antonio Zaffaroni','Area Coordinator – International Operations','Antonio Maria Zaffaroni, Engineer. My long career began in the Company in 1975, heading major foreign projects such as the Salto Grande dam in Uruguay, the Yacyretà dam on the border between Argentina and Paraguay and the Ertan dam in the Chinese region of Sichuan. In 2012 I accepted the position of Area Manager for the Americas, with the goal of contributing my experience to the growth of the company. My greatest satisfaction: maintaining excellent standards of performance when completing a large project.','"When I was six years old i said to my parents: I want to be an engineer when I grow up and travel around the world building dams".',
                                    'img/poster/poster_zaffaroni.png','people_2.html?people=zaffaroni&lang=en',
                                    'assets/images/people/antonio_zaffaroni.jpg','zaffaroni', new Date(2017,04,01), 'M', true));
personMapEn.set('assorati',new Person('Marco Assorati','Area Manager – International Operations','Marco Assorati, Civil Engineer. I have held important positions in the Company since 1998, handling the implementation of major works around the world: from the Kouroussa Kankan road project in Guinea Conakry, to Malaysia, where I am currently Project Manager for the Ulu Jelai hydroelectric project, after having worked on the Gibe I dam in Ethiopia, and the Gurara hydroelectric project in Nigeria. My long stay abroad has enabled me to understand how the ability to comprehend different cultures from our own is so important in this line of work.','“In our profession, we are exposed to lots of contacts with different people, cultures and religions, and we are used to overcoming many obstacles through dialogue.”',
                                    'img/poster/poster_assorati.jpg','people_2.html?people=assorati&lang=en',
                                    'assets/images/people/marco_assorati.jpg','assorati', new Date(2017,04,01), 'M', true));

personMapEn.set('zoppis',new Person('Eugenio Zoppis','Site Project Manager','Eugenio Zoppis, Civil Engineer. I began my career in the Company in 1988, quickly taking on the role of Project Manager for major road and hydroelectric projects in Africa. An entire career travelling in Sudan (Khartoum - Port Sudan Road), Ethiopia (Gibe I and Gibe II dams and tunnels) and Sierra Leone (Bumbuna dam), in addition to the colossal Gibe III dam in Ethiopia, which I have been overseeing as Project Manager since 2011. In exciting but difficult areas such as Ethiopia, dedication, organisation and problem solving skills really do make the difference, and these must become intrinsic qualities of anyone doing this work.','“We know we have made a change to the living conditions of an entire nation. First and foremost, we have brought employment, with projects of this scope employing some 6,000 to 7,000 individuals”.',
                                    'img/poster/poster_zoppis.png','people_2.html?people=zoppis&lang=en',
                                    'assets/images/people/eugenio_zoppis.jpg','zoppis', new Date(2017,04,01), 'M', true));
personMapEn.set('mariotti',new Person('Mirta Mariotti','Procurement Specialist','Mirta Mariotti, Architect from Rome. My experience in the Group began in 2007 as consultant on architectural issues, overseeing purchases of the architectural finishes of the Millennium Tower and Cultural Centre in Nigeria. In 2012, I moved to Copenhagen as Vendors Manager for the Cityringen Metro project, and then to Doha in Qatar for the Red Line North Underground project, as Procurement Specialist of electromechanical systems and architectural finishes. For a young person, working in a site abroad provides a unique opportunity for professional and personal growth.','“The company pays careful attention to young people. Experience in an international construction site is an absolute must: from the professional standpoint, it is the only way in which to truly grow”.',
                                    'img/poster/poster_mariotti.png','people_2.html?people=mariotti&lang=en',
                                    'assets/images/people/mirta_mariotti.jpg','mariotti', new Date(2017,04,01), 'M', true));
personMapEn.set('saraceni',new Person('Marco Saraceni','Branch Manager','Marco Saraceni, Degree in Civil Engineering from \"La Sapienza" University of Rome.  My experience in the implementation of large infrastructure projects started in 1987 with dam and road projects in Ethiopia, Sierra Leone, Zimbabwe, Guinea, Morocco, Algeria, Dubai, Kazakhstan and Slovakia. I took part in the study of tenders for large projects in the UAE, Algeria, Egypt, Croatia, Mozambique, Botswana, South Africa, Uganda, Sierra Leone and commercial activities in a number of countries. The Little Beles Dam in Ethiopia, the Bumbuna Dam in Sierra Leone, the Zhovhe and Osborne Dams in Zimbabwe, the Garafiri Dam in Guinea, the Rocade Méditerranéenne in Morocco, the motorway junction of Interchange no. 1 in Dubai are the main projects I’ve been involved with. In our line of work, listening, quick judgement and decision-making abilities are crucial for the successful completion of increasingly complex projects that require significant resources.','”A wonderful aspect of this work is complexity, which requires not only an individual response using one\'s own strengths, but the know-how of the entire team that is set up to complete a project.”',
                                    'img/poster/poster_saraceni.png','people_2.html?people=saraceni&lang=en',
                                    'assets/images/people/marco_saraceni.jpg','saraceni', new Date(2017,04,01), 'M', true));

    

personMapEn.set('cimiotti',new Person('Claudio Cimiotti','Senior Tunnel Engineer','Claudio Cimiotti, Management Engineer. I began my experience in the Company in 2006, after a brief internship in Cost Control. In just a few years, my rapid career growth brought me to manage two major sites in South America and the United States: the Mazar dam in Ecuador, as Head of Cost Control, and the Hydraulic Tunnel of Lake Mead, Intake No. 3 in Las Vegas, as Senior Tunnel Engineer. The technical skills I acquired in the years spent in America are now playing a fundamental role in achieving my personal and professional objectives.','“Lake Mead is a benchmark in the tunnelling sector worldwide. The hydro-geological difficulties and the pressures under which the TBM had to dig drove us to develop new technologies and new processes”.',
                                    'img/poster/poster_cimiotti.jpg','people_2.html?people=cimiotti&lang=en',
                                    'assets/images/people/claudio_cimiotti.jpg','cimiotti', new Date(2017,04,01), 'M', true));
personMapEn.set('leghissa',new Person('Luca Legisa Leghissa','Project Manager','Luca Legisa Leghissa, Engineer from Trieste. In 2006, I joined the Company\'s sales department, handling major projects in Nigeria (Abuja – Industrial Area of IDU), and in Denmark (Cityringen Metro), certainly the most satisfying project of my career. Since 2011, I\'ve been in Chile as Branch Manager, handling business development throughout Latin America. I am currently Project Manager for the Lima Line 2 Metro in Peru. The strongest bond is the professional one created within a team working on site.','“All of the people on site are essential for its proper functioning. In this environment, being part of a group is the basis for the success of any project”.',
                                    'img/poster/poster_leghissa.png','people_2.html?people=leghissa&lang=en',
                                    'assets/images/people/luca_legisa.jpg','leghissa', new Date(2017,04,01), 'M', true));





personMapEn.set('chirico',new Person('Federica Chirico','QHSE Support Officer','Federica Chirico. After graduating from Rome\'s La Sapienza University with a degree in Architecture, I obtained a Master\'s in Sustainable Buildings and Infrastructures from the Milan Polytechnic. During my studies, the Company selected me to oversee the New Kutaisi Bypass site in Georgia as Quality Manager, to then join headquarters to provide Quality, Environmental and Safety support for the Group’s Eastern European sites. By constantly facing professional challenges that require tenacity, dedication and expertise, we become more aware of our contribution in creating value for the populations of the world.','"Our motto is real: we really do build value. Value for the life and trade of the populations who will benefit from our work."',
                                    'img/poster/poster_chirico.jpg','people_2.html?people=chirico&lang=en',
                                    'assets/images/people/chirico_bg.png','chirico', new Date(2017,04,01), 'M', true));






function setupHomeCarousel(){
    badge = [];
    personMap.forEach(function(person){
        if (person.releaseDate.getTime() <= new Date().getTime() + 7 * ONE_DAY && !hasBadge(person) && !isComingSoon(person)) {
            peoples.push(person);
        } else if (person.releaseDate.getTime() <= new Date().getTime() + 7 * ONE_DAY && (hasBadge(person) || isComingSoon(person))) {
            badge.push(person);
        }
    });

    if (peoples.length %2 === 1) {
        peoples.push(unkPerson);
    }
    peoples = badge.reverse().concat(peoples);
    let index = 0;
    var element = document.getElementById("carousel-people-div");
    for(var i=1, len=peoples.length+1; i < len; i=i+2){
        var a = document.createElement("div");
        a.className = 'oggetto';
        var obj = peoples[i-1];
        var obj1 = peoples[i];

        createPeopleBox(obj,i,a);
        createPeopleBox(obj1,i+1,a);
        element.appendChild(a);
    }
}


function setupHomeCarouselEn(){
   badge = [];
    personMapEn.forEach(function(person){
        if (person.releaseDate.getTime() <= new Date().getTime() + 7 * ONE_DAY && !hasBadge(person) && !isComingSoon(person)) {
            peoples.push(person);
        } else if (person.releaseDate.getTime() <= new Date().getTime() + 7 * ONE_DAY && (hasBadge(person) || isComingSoon(person))) {
            badge.push(person);
        }
    });

    if (peoples.length %2 === 1) {
        peoples.push(unkPerson);
    }
    peoples = badge.concat(peoples);
    let index = 0;
    var element = document.getElementById("carousel-people-div");
    for(var i=1, len=peoples.length+1; i < len; i=i+2){
        var a = document.createElement("div");
        a.className = 'oggetto';
        var obj = peoples[i-1];
        var obj1 = peoples[i];

        createPeopleBox(obj,i,a);
        createPeopleBox(obj1,i+1,a);
        element.appendChild(a);
    }
}

function hasBadge(p) {
            if (p.releaseDate.getTime() < new Date().getTime() && new Date().getTime() < p.releaseDate.getTime()+7*ONE_DAY) {
                return true;
            }
            return false;
        }

function isComingSoon(person) {
    if (person.releaseDate < new Date() || new Date() <= new Date(person.releaseDate.getTime()-7*ONE_DAY)){
        return false;
    }
    return true;
}


function createPeopleBox(people,id,element){

    var extDiv = document.createElement("div");
        extDiv.id= 'ref-col';
        extDiv.className = 'item';
    var a = document.createElement("a");
        a.href = people.link;
        
    var container = document.createElement("div");
        container.className = 'people-container';
    var newBadge = document.createElement("img");
        newBadge.src = "assets/images/people/new_badge.png";
        newBadge.className = 'new_badge';
        var x7 = document.createElement("div");
        x7.className = 'col-xs-5';
        container.appendChild(x7);
        var x5 = document.createElement("div");
        x5.className = 'col-xs-7 no-pad';
        container.appendChild(x5);
        var infoDiv = document.createElement("div");
        infoDiv.style.width = '100%';
        var name = document.createElement("span");
        name.className = 'people_name';
        var title = document.createElement("span");
        title.className = 'people_title';
        if(people.releaseDate < new Date()){
          name.innerHTML = people.name;
          title.innerHTML = people.role;
          container.id = 'peo_bg_'+people.shortcut;
          if(hasBadge(people)) {
            a.appendChild(newBadge);
          }
        }else{
          name.innerHTML = 'Coming soon';
          container.id = 'peo_bg_hide_'+people.shortcut;
          title.innerHTML = 'Available from ' + people.releaseDate.getDate() + '/' + (people.releaseDate.getMonth()+1);
        }

        infoDiv.appendChild(name);
        infoDiv.appendChild(title);

        x5.appendChild(infoDiv);
        if(people.releaseDate < new Date() && people.link !== ''){
            a.appendChild(container);
            extDiv.appendChild(a);
        }else{
            extDiv.appendChild(container);
        }

        
        element.appendChild(extDiv);
}
