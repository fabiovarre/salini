var params = {};

if (location.search) {
    var parts = location.search.substring(1).split('&');

    for (var i = 0; i < parts.length; i++) {
        var nv = parts[i].split('=');
        if (!nv[0]) continue;
        params[nv[0]] = nv[1] || true;
    }
}

function getLanguage() {
    if(params.lang) {
        return params.lang;
    }
    return 'it';
}

function getPeopleLang() {
    return params.people;
}

function getBgType() {
    if(params.type) {
        return params.type;
    }
    return 'ponte';
}
