/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function setVideoProperties(id1,id2,s1,s2,poster){
    $(id).attr('poster', poster);
    var video = document.getElementById(id);
    var sources = video.getElementsByTagName('source');
    sources[0].src = s1;
    sources[1].src = s2;
    video.load();
}

function setupVideoInfo(poster,s0,s1){
    setPoster(poster);
    var video = document.getElementById('videoPeople');
    var sources = video.getElementsByTagName('source');
    sources[0].src = s0;
    sources[1].src = s1;


    video.load();
    var videoSm = document.getElementById('videoPeopleSm');
            var sources = videoSm.getElementsByTagName('source');
            sources[0].src = s0;
            sources[1].src = s1;

            videoSm.load();
}

function setPoster(poster){
    $('#videoPeople').attr('poster', poster);
    $('#videoPeopleSm').attr('poster', poster);
}

function setupPerson(person){
    $('#peopleName').html(person.name);
    $('#peopleRole').html(person.role);
    $('#peopleStory').html(person.story);
    $('#peopleQuote').html(person.quote);
}

function setupOpere(opere){
    for(var i=1, len=opere.length+1; i < len; i++){
        var obj = opere[i-1];
        if(obj.zappar != null){
           $('#imgOpera'+i).attr('src',obj.zappar);
        }else{
            $('#imgOpera'+i).attr('src',obj.image);
        }
            $('#luogoOpera'+i).html(obj.place);
            $('#nomeOpera'+i).html(obj.name +', '+ obj.year);
            $('#opera-link'+i).attr('href',obj.link);

    }
};

function setupLinkedPeople(people){
    generateCarouselPeople(people);

    for(var i=1, len=people.length+1; i < len; i++){
        var obj = people[i-1];
        $('#people'+i).attr('href',obj.link);


    }
    getCarousel();

}

function generateCarouselPeople(people){
    var element = document.getElementById("people-linked-carousel");
    for(var i=1, len=people.length+1; i < len; i++){
        var obj = people[i-1];
        var divExternal = document.createElement("a");
        divExternal.id = 'people'+i;
        divExternal.className = 'people-container item '+ obj.shortcut;
        var link = document.createElement("a");
        link.id = 'people-link-'+i;
       // if (getLanguage() === 'it') {
            link.href= obj.link ;
       // } else {
       //     link.href = obj.link+'&lang=en';
        //    divExternal.href = obj.link+'&lang=en';
        //}
        var x7 = document.createElement("div");
        x7.className = 'col-xs-5';
        link.appendChild(x7);
        var x5 = document.createElement("div");
        x5.className = 'col-xs-7 no-pad';
        link.appendChild(x5);
        var infoDiv = document.createElement("div");
        infoDiv.style.width = '100%';
        var name = document.createElement("span");
        name.className = 'people_name';
        name.innerHTML = obj.name;
        var title = document.createElement("span");
        title.className = 'people_title';
        title.innerHTML = obj.role;
        infoDiv.appendChild(name);
        infoDiv.appendChild(title);
        x5.appendChild(infoDiv);


        divExternal.appendChild(link);
        element.appendChild(divExternal);
    }

}


function getCarousel(){
     $('#people-linked-carousel').owlCarousel({
                       itemsCustom : [
                      [0, 1],
                      [450, 1],
                      [600, 2],
                      [700, 2],
                      [1000, 3],
                      [1200, 3],
                      [1400, 4],
                      [1600, 4]
                    ],
                    navigation : false
                    });
}

function setLink(people) {
    $('#link-it').attr('href','people_2.html?people='+people+'&lang=it');
    $('#link-en').attr('href','people_2.html?people='+people+'&lang=en');
}

function setLinkHome(lang) {
    $('#logo-link').attr('href','index4.html?&lang='+lang);
}

function setTabTitle(lang) {
    if(lang === 'it') {
        $('#int-a').html("intervista");
        $('#exp-a').html("esperienza");
        $('#bui-a').html("opere");
        
    }else{
        $('#int-a').html("interview");
        $('#exp-a').html("experience");
        $('#bui-a').html("projects");
    }
}

function setDisclaimer(people, lang) {
    let p = personMap.get(people);
    if (p.sov1) {
        if(lang === 'it') {
            $('#disclaimer').html("Intervista del giugno 2015");
            if (p.shortcut === 'quarta') {
                $('#major').html("oggi EVP Major Projects LANE");
            }
        } else {
            $('#disclaimer').html("June 2015 Interview");
            if (p.shortcut === 'quarta') {
                $('#major').html("today EVP Major Projects LANE");
            }
        }
    }
    console.log(p, 'p');
}


function getPeople(myPeople){
    switch(myPeople){
        case 'saraceni':
            var saraceni = personMap.get('saraceni');
            setupPerson(saraceni);
            setupVideoInfo(saraceni.poster,'video/saraceni_long_new.mp4','video/saraceni_long_new.ogv');
            setupOpere(
                    [catalogoOpere.get('ZhovheDam'),
                     catalogoOpere.get('RocadeMediterranenne'),
                     catalogoOpere.get('AutostradaD1'),
                     catalogoOpere.get('Osborne'),
                     catalogoOpere.get('DubaiRas'),
                     catalogoOpere.get('CityringenMetro')]);
            setupLinkedPeople([personMap.get('quarta'),personMap.get('assorati'),personMap.get('mariotti'),personMap.get('cimiotti')]);

            break;
        case 'zaffaroni':
            var zaffaroni = personMap.get('zaffaroni');
            setupPerson(zaffaroni);
            setupVideoInfo(zaffaroni.poster,'video/zaffaroni_long_new.mp4','video/zaffaroni_long_new.ogv');
            setupOpere(
                    [catalogoOpere.get('YaciretaDam'),
                     catalogoOpere.get('ErtanDam'),
                     catalogoOpere.get('PiedraAguila'),
                     catalogoOpere.get('Anchieta'),
                     catalogoOpere.get('Osborne'),
                     catalogoOpere.get('DubaiRas'),
                     ]);
            setupLinkedPeople([personMap.get('quarta'),personMap.get('carlesimo'),personMap.get('cimiotti'),personMap.get('zoppis')]);
            break;

        case 'cimiotti':
            var cimiotti = personMap.get('cimiotti');
            setupPerson(cimiotti);
            setupVideoInfo(cimiotti.poster,'video/cimiotti_long_new.mp4','video/cimiotti_long_new.ogv');
            setupOpere(
                    [catalogoOpere.get('NathpaJhakri'),
                     catalogoOpere.get('LakeMead'),
                     catalogoOpere.get('MazarDam'),
                     catalogoOpere.get('ErtanDam'),
                     catalogoOpere.get('Anacostia'),
                     catalogoOpere.get('Dugway')]);
            setupLinkedPeople([personMap.get('mariotti'),personMap.get('leghissa'),personMap.get('quarta'),personMap.get('assorati')]);
            break;

        case 'zoppis':
            var zoppis = personMap.get('zoppis');
            setupPerson(zoppis);
            setupVideoInfo(zoppis.poster,'video/zoppis_long_new.mp4','video/zoppis_long_new.ogv');
            setupOpere(
                    [catalogoOpere.get('Khartoum'),
                     catalogoOpere.get('GibeII'),
                     catalogoOpere.get('GibeIII'),
                     catalogoOpere.get('Bumbuna'),
                     catalogoOpere.get('LakeMead'),
                     catalogoOpere.get('MazarDam')]);
            setupLinkedPeople([personMap.get('saraceni'),personMap.get('assorati'),personMap.get('leghissa'),personMap.get('chirico')]);
            break;

        case 'quarta':
            var quarta = personMap.get('quarta');
            setupPerson(quarta);
            setupVideoInfo(quarta.poster,'video/quarta_long_new.mp4','video/quarta_long_new.ogv')
            setupOpere(
                    [catalogoOpere.get('ErtanDam'),
                     catalogoOpere.get('Panama'),
                     catalogoOpere.get('NathpaJhakri'),
                     catalogoOpere.get('Portland'),
                     catalogoOpere.get('Khartoum'),
                     catalogoOpere.get('GibeII')]);
            setupLinkedPeople([personMap.get('carlesimo'),personMap.get('mariotti'),personMap.get('zaffaroni'),personMap.get('zoppis')]);
            break;

        case 'carlesimo':
            var carlesimo = personMap.get('carlesimo');
            setupPerson(carlesimo);
            setupVideoInfo(carlesimo.poster,'video/carlesimo_long_new.mp4','video/carlesimo_long_new.ogv');
            setupOpere(
                    [catalogoOpere.get('YaciretaDam'),
                     catalogoOpere.get('GuraraDam'),
                     catalogoOpere.get('GarafiriDam'),
                     catalogoOpere.get('Alwahda'),
                     catalogoOpere.get('Neckartal'),
                     catalogoOpere.get('Abuja')]);
            setupLinkedPeople([personMap.get('zaffaroni'),personMap.get('assorati'),personMap.get('quarta'),personMap.get('cimiotti')]);
            break;

        case 'leghissa':
            var leghissa = personMap.get('leghissa');
            setupPerson(leghissa);
            setupVideoInfo(leghissa.poster,'video/leghissa_long_new.mp4','video/leghissa_long_new.ogv');
            setupOpere([catalogoOpere.get('Abuja'),
                        catalogoOpere.get('CityringenMetro'),
                        catalogoOpere.get('Sogamoso'),
                        catalogoOpere.get('YaciretaDam'),
                        catalogoOpere.get('GuraraDam'),
                        catalogoOpere.get('Quimbo')]);
            setupLinkedPeople([personMap.get('zoppis'),personMap.get('saraceni'),personMap.get('cimiotti'),personMap.get('quarta')]);
            break;

        case 'assorati':
            var assorati = personMap.get('assorati');
            setupPerson(assorati);
            setupVideoInfo(assorati.poster,'video/assorati_long_new.mp4','video/assorati_long_new.ogv');
            setupOpere([
                catalogoOpere.get('Kouroussa'),
                catalogoOpere.get('UluJelai'),
                catalogoOpere.get('GibeI'),
                catalogoOpere.get('AbujaMillenial'),
                catalogoOpere.get('GuraraDam'),
                catalogoOpere.get('CityringenMetro')]);
            setupLinkedPeople([personMap.get('quarta'),personMap.get('zaffaroni'),personMap.get('mariotti'),personMap.get('chirico')]);
            break;

        case 'mariotti':
            var mariotti = personMap.get('mariotti');
            setupPerson(mariotti);
            setupVideoInfo(mariotti.poster,'video/mariotti_long_new.mp4','video/mariotti-long-new.ogv');
            setupOpere([
                catalogoOpere.get('CityringenMetro'),
                catalogoOpere.get('DohaRedLine'),
                catalogoOpere.get('AbujaMillenial'),
                catalogoOpere.get('UluJelai'),
                catalogoOpere.get('Idu'),
                catalogoOpere.get('Kouroussa')]);
            setupLinkedPeople([personMap.get('zoppis'),personMap.get('leghissa'),personMap.get('assorati'),personMap.get('chirico')]);
            break;

            case 'chirico':
                var chirico = personMap.get('chirico');
                setupPerson(chirico);
                setupVideoInfo(chirico.poster,'video/chirico_long_new.mp4','video/chirico-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('Kouroussa'),
                    catalogoOpere.get('UluJelai'),
                    catalogoOpere.get('AbujaMillenial'),
                    catalogoOpere.get('AutostradaD1'),
                    catalogoOpere.get('Idu'),
                    catalogoOpere.get('Panama')]);
                setupLinkedPeople([personMap.get('zoppis'),personMap.get('leghissa'),personMap.get('mariotti'),personMap.get('saraceni')]);
                break;
            
            case 'albieri':
                var albieri = personMap.get('albieri');
                setupPerson(albieri);
                setupVideoInfo(albieri.poster, 'video/albieri_long_new.mp4', 'video/albieri-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('Panama'),
                    catalogoOpere.get('Gerdp'),
                    catalogoOpere.get('DohaRedLine'),
                    catalogoOpere.get('Stavros'),
                    catalogoOpere.get('Koysha'),
                    catalogoOpere.get('Alkhor')
                ]);
                setupLinkedPeople([personMap.get('carlesimo'),personMap.get('assorati'),personMap.get('mariotti'),personMap.get('zoppis')]);

                break;
            
            case 'fabbri':
            var fabbri = personMap.get('fabbri');
            setupPerson(fabbri);
            setupVideoInfo(fabbri.poster, 'video/fabbri_long_new.mp4', 'video/fabbri-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('Bumbuna'),
                    catalogoOpere.get('CityringenMetro'),
                    catalogoOpere.get('TanaBeles'),
                    catalogoOpere.get('RomeMb1'),
                    catalogoOpere.get('Osborne'),
                    catalogoOpere.get('Rogun')
                ]);
                setupLinkedPeople([personMap.get('cimiotti'),personMap.get('albieri'),personMap.get('saraceni'),personMap.get('zaffaroni')]);
            break;

            case 'catrini':
            var catrini = personMap.get('catrini');
            setupPerson(catrini);
            setupVideoInfo(catrini.poster, 'video/catrini_long_new.mp4', 'video/catrini-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('Tarbela'),
                    catalogoOpere.get('Ghazi'),
                    catalogoOpere.get('Khali'),
                    catalogoOpere.get('ErtanDam'),
                    catalogoOpere.get('Xiaolangdi'),
                    catalogoOpere.get('Beltway')
                ]);
            setupLinkedPeople([personMap.get('assorati'),personMap.get('fabbri'),personMap.get('albieri'),personMap.get('cimiotti')]);
            break;

            case 'blanda':
            var blanda = personMap.get('blanda');
            setupPerson(blanda);
            setupVideoInfo(blanda.poster, 'video/blanda_long_new.mp4', 'video/blanda-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('CityringenMetro'),
                    catalogoOpere.get('GibeI'),
                    catalogoOpere.get('Bujagali'),
                    catalogoOpere.get('DohaRedLine'),
                    catalogoOpere.get('RiadMetro'),
                    catalogoOpere.get('GibeII')
                ]);
                setupLinkedPeople([personMap.get('catrini'),personMap.get('fabbri'),personMap.get('albieri'),personMap.get('chirico')]);
            break;

            case 'mirabelli':
            var mirabelli = personMap.get('mirabelli');
            setupPerson(mirabelli);
            setupVideoInfo(mirabelli.poster, 'video/mirabelli_long_new.mp4', 'video/mirabelli-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('TanaBeles'),
                    catalogoOpere.get('Karameh'),
                    catalogoOpere.get('Kafrein'),
                    catalogoOpere.get('Larache'),
                    catalogoOpere.get('GibeIII'),
                    catalogoOpere.get('Legadadi')
                ]);
                setupLinkedPeople([personMap.get('blanda'),personMap.get('catrini'),personMap.get('fabbri'),personMap.get('albieri')]);
            break;

            case 'basso':
            var basso = personMap.get('basso');
            setupPerson(basso);
            setupVideoInfo(basso.poster, 'video/basso_long_new.mp4', 'video/basso-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('RiadMetro'),
                    catalogoOpere.get('SidneyMetro'),
                    catalogoOpere.get('Koysha'),
                    catalogoOpere.get('GibeIII'),
                    catalogoOpere.get('Maxbrewer'),
                    catalogoOpere.get('AbujaMillenial')
                ]);
                setupLinkedPeople([personMap.get('blanda'),personMap.get('catrini'),personMap.get('fabbri'),personMap.get('albieri')]);
            break;

            case 'laterza':
            var laterza = personMap.get('laterza');
            setupPerson(laterza);
            setupVideoInfo(laterza.poster, 'video/laterza_long_new.mp4', 'video/laterza-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('A4Motorway'),
                    catalogoOpere.get('ThirdLaneA4'),
                    catalogoOpere.get('Jonica'),
                    catalogoOpere.get('Brennero'),
                    catalogoOpere.get('SalernoRc'),
                    catalogoOpere.get('Pedemontana')
                ]);
            setupLinkedPeople([personMap.get('albieri'),personMap.get('blanda'),personMap.get('catrini'),personMap.get('fabbri')]);
            break;

            case 'buffa':
            var buffa = personMap.get('buffa');
            setupPerson(buffa);
            setupVideoInfo(buffa.poster, 'video/buffa_long_new.mp4', 'video/buffa-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('Panama'),
                    catalogoOpere.get('PiedraAguila'),
                    catalogoOpere.get('BolognaFirenze'),
                    catalogoOpere.get('LakeMead'),
                    catalogoOpere.get('YaciretaDam'),
                    catalogoOpere.get('Karahnjukar')
                ]);
            setupLinkedPeople([personMap.get('laterza'),personMap.get('albieri'),personMap.get('blanda'),personMap.get('catrini')]);
            break;

            case 'ferrara':
            var ferrara = personMap.get('ferrara');
            setupPerson(ferrara);
            setupVideoInfo(ferrara.poster, 'video/ferrara_long_new.mp4', 'video/ferrara-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('AbuSimbel'),
                    catalogoOpere.get('Tripoli'),
                    catalogoOpere.get('UniMisurata'),
                    catalogoOpere.get('PalazzoLombardia'),
                    catalogoOpere.get('RocadeMediterranenne'),
                    catalogoOpere.get('RasEjdyerEmssad')
                ]);
            setupLinkedPeople([personMap.get('buffa'),personMap.get('laterza'),personMap.get('basso'),personMap.get('blanda')]);
            break;

            case 'fiori':
            var fiori = personMap.get('fiori');
            setupPerson(fiori);
            setupVideoInfo(fiori.poster, 'video/fiori_long_new.mp4', 'video/fiori-long-new.ogv');
                setupOpere([
                    catalogoOpere.get('UniCalabria'),
                    catalogoOpere.get('GenovaMetro'),
                    catalogoOpere.get('NapoliMetro'),
                    catalogoOpere.get('TorinoMilano'),
                    catalogoOpere.get('Pedemontana'),        
                    catalogoOpere.get('SalernoRc')
                ]);
                setupLinkedPeople([personMap.get('basso'),personMap.get('buffa'),personMap.get('laterza'),personMap.get('basso')]);
            break;

        default:
            window.location.href = "index4.html";
    }
}







function getPeopleEn(myPeople){
    console.log('Start get people En');
    switch(myPeople){
        case 'saraceni':
            var saraceni = personMapEn.get('saraceni');
            setupPerson(saraceni);
            setupVideoInfo(saraceni.poster,'video/saraceni_long_new_en.mp4','video/saraceni_long_new_en.ogv');
            setupOpere(
                    [catalogoOpereEn.get('ZhovheDam'),
                     catalogoOpereEn.get('RocadeMediterranenne'),
                     catalogoOpereEn.get('AutostradaD1'),
                     catalogoOpereEn.get('Osborne'),
                     catalogoOpereEn.get('DubaiRas'),
                     catalogoOpereEn.get('CityringenMetro')]);
            setupLinkedPeople([personMapEn.get('quarta'),personMapEn.get('assorati'),personMapEn.get('mariotti'),personMapEn.get('cimiotti')]);


            break;
        case 'zaffaroni':
            var zaffaroni = personMapEn.get('zaffaroni');
            setupPerson(zaffaroni);
            setupVideoInfo(zaffaroni.poster,'video/zaffaroni_long_new_en.mp4','video/zaffaroni_long_new_en.ogv');
            setupOpere(
                    [catalogoOpereEn.get('YaciretaDam'),
                     catalogoOpereEn.get('ErtanDam'),
                     catalogoOpereEn.get('PiedraAguila'),
                     catalogoOpereEn.get('Anchieta'),
                     catalogoOpereEn.get('Osborne'),
                     catalogoOpereEn.get('DubaiRas'),
                     ]);
            setupLinkedPeople([personMapEn.get('quarta'),personMapEn.get('carlesimo'),personMapEn.get('cimiotti'),personMapEn.get('zoppis')]);
            break;

        case 'cimiotti':
            var cimiotti = personMapEn.get('cimiotti');
            setupPerson(cimiotti);
            setupVideoInfo(cimiotti.poster,'video/cimiotti_long_new_en.mp4','video/cimiotti_long_new_en.ogv');
            setupOpere(
                    [catalogoOpereEn.get('NathpaJhakri'),
                     catalogoOpereEn.get('LakeMead'),
                     catalogoOpereEn.get('MazarDam'),
                     catalogoOpereEn.get('ErtanDam'),
                     catalogoOpereEn.get('Anacostia'),
                     catalogoOpereEn.get('Dugway')]);
            setupLinkedPeople([personMapEn.get('mariotti'),personMapEn.get('leghissa'),personMapEn.get('quarta'),personMapEn.get('assorati')]);
            break;

        case 'zoppis':
            var zoppis = personMapEn.get('zoppis');
            setupPerson(zoppis);
            setupVideoInfo(zoppis.poster,'video/zoppis_long_new_en.mp4','video/zoppis_long_new_en.ogv');
            setupOpere(
                    [catalogoOpereEn.get('Khartoum'),
                     catalogoOpereEn.get('GibeII'),
                     catalogoOpereEn.get('GibeIII'),
                     catalogoOpereEn.get('Bumbuna'),
                     catalogoOpereEn.get('LakeMead'),
                     catalogoOpereEn.get('MazarDam')]);
            setupLinkedPeople([personMapEn.get('saraceni'),personMapEn.get('assorati'),personMapEn.get('leghissa'),personMapEn.get('chirico')]);
            break;

        case 'quarta':
            var quarta = personMapEn.get('quarta');
            setupPerson(quarta);
            setupVideoInfo(quarta.poster,'video/quarta_long_new_en.mp4','video/quarta_long_new_en.ogv')
            setupOpere(
                    [catalogoOpereEn.get('ErtanDam'),
                     catalogoOpereEn.get('Panama'),
                     catalogoOpereEn.get('NathpaJhakri'),
                     catalogoOpereEn.get('Portland'),
                     catalogoOpereEn.get('Khartoum'),
                     catalogoOpereEn.get('GibeII')]);
            setupLinkedPeople([personMapEn.get('carlesimo'),personMapEn.get('mariotti'),personMapEn.get('zaffaroni'),personMapEn.get('zoppis')]);
            break;

        case 'carlesimo':
            var carlesimo = personMapEn.get('carlesimo');
            setupPerson(carlesimo);
            setupVideoInfo(carlesimo.poster,'video/carlesimo_long_new_en.mp4','video/carlesimo_long_new_en.ogv');
            setupOpere(
                    [catalogoOpereEn.get('YaciretaDam'),
                     catalogoOpereEn.get('GuraraDam'),
                     catalogoOpereEn.get('GarafiriDam'),
                     catalogoOpereEn.get('Alwahda'),
                     catalogoOpereEn.get('Neckartal'),
                     catalogoOpereEn.get('Abuja')]);
            setupLinkedPeople([personMapEn.get('zaffaroni'),personMapEn.get('assorati'),personMapEn.get('quarta'),personMapEn.get('cimiotti')]);
            break;

        case 'leghissa':
            var leghissa = personMapEn.get('leghissa');
            setupPerson(leghissa);
            setupVideoInfo(leghissa.poster,'video/leghissa_long_new_en.mp4','video/leghissa_long_new_en.ogv');
            setupOpere([catalogoOpereEn.get('Abuja'),
                        catalogoOpereEn.get('CityringenMetro'),
                        catalogoOpereEn.get('Sogamoso'),
                        catalogoOpereEn.get('YaciretaDam'),
                        catalogoOpereEn.get('GuraraDam'),
                        catalogoOpereEn.get('Quimbo')]);
            setupLinkedPeople([personMapEn.get('zoppis'),personMapEn.get('saraceni'),personMapEn.get('cimiotti'),personMapEn.get('quarta')]);
            break;

        case 'assorati':
            var assorati = personMapEn.get('assorati');
            setupPerson(assorati);
            setupVideoInfo(assorati.poster,'video/assorati_long_new_en.mp4','video/assorati_long_new_en.ogv');
            setupOpere([
                catalogoOpereEn.get('Kouroussa'),
                catalogoOpereEn.get('UluJelai'),
                catalogoOpereEn.get('GibeI'),
                catalogoOpereEn.get('AbujaMillenial'),
                catalogoOpereEn.get('GuraraDam'),
                catalogoOpereEn.get('CityringenMetro')]);
            setupLinkedPeople([personMapEn.get('quarta'),personMapEn.get('zaffaroni'),personMapEn.get('mariotti'),personMapEn.get('chirico')]);
            break;

        case 'mariotti':
            var mariotti = personMapEn.get('mariotti');
            setupPerson(mariotti);
            setupVideoInfo(mariotti.poster,'video/mariotti_long_new_en.mp4','video/mariotti-long-new_en.ogv');
            setupOpere([
                catalogoOpereEn.get('CityringenMetro'),
                catalogoOpereEn.get('DohaRedLine'),
                catalogoOpereEn.get('AbujaMillenial'),
                catalogoOpereEn.get('UluJelai'),
                catalogoOpereEn.get('Idu'),
                catalogoOpereEn.get('Kouroussa')]);
            setupLinkedPeople([personMapEn.get('zoppis'),personMapEn.get('leghissa'),personMapEn.get('assorati'),personMapEn.get('chirico')]);
            break;

            case 'chirico':
                var chirico = personMapEn.get('chirico');
                setupPerson(chirico);
                setupVideoInfo(chirico.poster,'video/chirico_long_new_en.mp4','video/chirico-long-new_en.ogv');
                setupOpere([
                    catalogoOpereEn.get('Kouroussa'),
                    catalogoOpereEn.get('UluJelai'),
                    catalogoOpereEn.get('AbujaMillenial'),
                    catalogoOpereEn.get('AutostradaD1'),
                    catalogoOpereEn.get('Idu'),
                    catalogoOpereEn.get('Panama')]);
                setupLinkedPeople([personMapEn.get('zoppis'),personMapEn.get('leghissa'),personMapEn.get('mariotti'),personMapEn.get('saraceni')]);
                break;
            
            case 'albieri':
                var albieri = personMapEn.get('albieri');
                setupPerson(albieri);
                setupVideoInfo(albieri.poster, 'video/albieri_long_new_en.mp4', 'video/albieri-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('Panama'),
                    catalogoOpereEn.get('Gerdp'),
                    catalogoOpereEn.get('DohaRedLine'),
                    catalogoOpereEn.get('Stavros'),
                    catalogoOpereEn.get('Koysha'),
                    catalogoOpereEn.get('Alkhor')
                ]);
                setupLinkedPeople([personMapEn.get('carlesimo'),personMapEn.get('assorati'),personMapEn.get('mariotti'),personMapEn.get('zoppis')]);

                break;
            
            case 'fabbri':
            var fabbri = personMapEn.get('fabbri');
            setupPerson(fabbri);
            setupVideoInfo(fabbri.poster, 'video/fabbri_long_new_en.mp4', 'video/fabbri-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('Bumbuna'),
                    catalogoOpereEn.get('CityringenMetro'),
                    catalogoOpereEn.get('TanaBeles'),
                    catalogoOpereEn.get('RomeMb1'),
                    catalogoOpereEn.get('Osborne'),
                    catalogoOpereEn.get('Rogun')
                ]);
                setupLinkedPeople([personMapEn.get('cimiotti'),personMapEn.get('albieri'),personMapEn.get('saraceni'),personMapEn.get('zaffaroni')]);
            break;

            case 'catrini':
            var catrini = personMapEn.get('catrini');
            setupPerson(catrini);
            setupVideoInfo(catrini.poster, 'video/catrini_long_new_en.mp4', 'video/catrini-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('Tarbela'),
                    catalogoOpereEn.get('Ghazi'),
                    catalogoOpereEn.get('Khali'),
                    catalogoOpereEn.get('ErtanDam'),
                    catalogoOpereEn.get('Xiaolangdi'),
                    catalogoOpereEn.get('Beltway')
                ]);
            setupLinkedPeople([personMapEn.get('assorati'),personMapEn.get('fabbri'),personMapEn.get('albieri'),personMapEn.get('cimiotti')]);
            break;

            case 'blanda':
            var blanda = personMapEn.get('blanda');
            setupPerson(blanda);
            setupVideoInfo(blanda.poster, 'video/blanda_long_new_en.mp4', 'video/blanda-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('CityringenMetro'),
                    catalogoOpereEn.get('GibeI'),
                    catalogoOpereEn.get('Bujagali'),
                    catalogoOpereEn.get('DohaRedLine'),
                    catalogoOpereEn.get('RiadMetro'),
                    catalogoOpereEn.get('GibeII')
                ]);
                setupLinkedPeople([personMapEn.get('catrini'),personMapEn.get('fabbri'),personMapEn.get('albieri'),personMapEn.get('chirico')]);
            break;

            case 'mirabelli':
            var mirabelli = personMapEn.get('mirabelli');
            setupPerson(mirabelli);
            setupVideoInfo(mirabelli.poster, 'video/mirabelli_long_new_en.mp4', 'video/mirabelli-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('TanaBeles'),
                    catalogoOpereEn.get('Karameh'),
                    catalogoOpereEn.get('Kafrein'),
                    catalogoOpereEn.get('Larache'),
                    catalogoOpereEn.get('GibeIII'),
                    catalogoOpereEn.get('Legadadi')
                ]);
                setupLinkedPeople([personMapEn.get('blanda'),personMapEn.get('catrini'),personMapEn.get('fabbri'),personMapEn.get('albieri')]);
            break;

            case 'basso':
            var basso = personMapEn.get('basso');
            setupPerson(basso);
            setupVideoInfo(basso.poster, 'video/basso_long_new_en.mp4', 'video/basso-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('RiadMetro'),
                    catalogoOpereEn.get('SidneyMetro'),
                    catalogoOpereEn.get('Koysha'),
                    catalogoOpereEn.get('GibeIII'),
                    catalogoOpereEn.get('Maxbrewer'),
                    catalogoOpereEn.get('AbujaMillenial')
                ]);
                setupLinkedPeople([personMapEn.get('blanda'),personMapEn.get('catrini'),personMapEn.get('fabbri'),personMapEn.get('albieri')]);
            break;

            case 'laterza':
            var laterza = personMapEn.get('laterza');
            setupPerson(laterza);
            setupVideoInfo(laterza.poster, 'video/laterza_long_new_en.mp4', 'video/laterza-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('A4Motorway'),
                    catalogoOpereEn.get('ThirdLaneA4'),
                    catalogoOpereEn.get('Jonica'),
                    catalogoOpereEn.get('Brennero'),
                    catalogoOpereEn.get('SalernoRc'),
                    catalogoOpereEn.get('Pedemontana')
                ]);
            setupLinkedPeople([personMapEn.get('albieri'),personMapEn.get('blanda'),personMapEn.get('catrini'),personMapEn.get('fabbri')]);
            break;

            case 'buffa':
            var buffa = personMapEn.get('buffa');
            setupPerson(buffa);
            setupVideoInfo(buffa.poster, 'video/buffa_long_new_en.mp4', 'video/buffa-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('Panama'),
                    catalogoOpereEn.get('PiedraAguila'),
                    catalogoOpereEn.get('BolognaFirenze'),
                    catalogoOpereEn.get('LakeMead'),
                    catalogoOpereEn.get('YaciretaDam'),
                    catalogoOpereEn.get('Karahnjukar')
                ]);
            setupLinkedPeople([personMapEn.get('laterza'),personMapEn.get('albieri'),personMapEn.get('blanda'),personMapEn.get('catrini')]);
            break;

            case 'ferrara':
            var ferrara = personMapEn.get('ferrara');
            setupPerson(ferrara);
            setupVideoInfo(ferrara.poster, 'video/ferrara_long_new_en.mp4', 'video/ferrara-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('AbuSimbel'),
                    catalogoOpereEn.get('Tripoli'),
                    catalogoOpereEn.get('UniMisurata'),
                    catalogoOpereEn.get('PalazzoLombardia'),
                    catalogoOpereEn.get('RocadeMediterranenne'),
                    catalogoOpereEn.get('RasEjdyerEmssad')
                ]);
            setupLinkedPeople([personMapEn.get('buffa'),personMapEn.get('laterza'),personMapEn.get('basso'),personMapEn.get('blanda')]);
            break;

            case 'fiori':
            var fiori = personMapEn.get('fiori');
            setupPerson(fiori);
            setupVideoInfo(fiori.poster, 'video/fiori_long_new_en.mp4', 'video/fiori-long-new.ogv');
                setupOpere([
                    catalogoOpereEn.get('UniCalabria'),
                    catalogoOpereEn.get('GenovaMetro'),
                    catalogoOpereEn.get('NapoliMetro'),
                    catalogoOpereEn.get('TorinoMilano'),
                    catalogoOpereEn.get('Pedemontana'),        
                    catalogoOpereEn.get('SalernoRc')
                ]);
                setupLinkedPeople([personMapEn.get('basso'),personMapEn.get('buffa'),personMapEn.get('laterza'),personMapEn.get('basso')]);
            break;


        default:
            window.location.href = "index4.html";
    }
}



var Opera = function(image,place,name,link){
    this.image = image;
    this.place = place;
    this.name = name;
    this.link = link;
};
